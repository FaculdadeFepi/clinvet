import React from 'react';
//import logo from '../images/logo-nav.png';
import {Link} from 'react-router-dom';


const NavBar = () => {
  return (
    <nav class="container-fluid navbar navbar-fixed-top navbar-expand-lg navbar-dark bg-faded d-inline-block align-top" style={{ backgroundColor:'#005284'}}>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
          <li class="nav-item active">
          <Link to='/' class="navbar-brand">Home</Link>
          </li>
          <li class="nav-item active">
          <Link to='/Store' class="navbar-brand">Produtos</Link>
          </li>
          <li class="nav-item active">
          <Link to='/Contato' class="navbar-brand">Contato</Link>
          </li>
        </ul>
        <form class="form-inline my-2 my-lg-0">
          <input class="form-control mr-sm-2" type="search" aria-label="Search" />
          <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Localizar</button>
        </form>
      </div>
    </nav>
  );
};

export default NavBar;
