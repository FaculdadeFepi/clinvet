import React, { Component }from 'react';
//import logo from './images/logo-nav.png';
import './App.css';
import NavBar from './ui/NavBar';
import Footer from './Container/Footer';

class App extends Component {
  render(){
    return (
      <div>
        <div>
          <NavBar/>
        </div>
        <div>
          {this.props.children}
        </div>
        <div>
        <Footer />
        </div>
      </div>
    );
  }
}

export default App;
