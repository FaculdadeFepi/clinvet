import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Contato from './Container/Contato';
import Sobre from './Container/Sobre';
import Home from './Container/Home';
import Store from './Container/Store';

const routes = (
    <App>
        <Switch>
            <Route exact path="/" component={Home} />
            <Route path="/Store" component={Store} />
            <Route path="/Sobre" component={Sobre} />
            <Route path="/Contato" component={Contato} />
        </Switch>
    </App>
);

ReactDOM.render(
        (
            <Router>
                {routes}
            </Router>
        )
    , 
    
    document.getElementById('root'));

serviceWorker.unregister();
