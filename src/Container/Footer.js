import React from "react";
import logo from '../images/Logo/logo-svg.png';
import {Link} from 'react-router-dom';
import { FaPhone, FaMailBulk, FaFacebook } from 'react-icons/fa';
import { IoLogoWhatsapp, IoLogoTwitter } from "react-icons/io";
import { GoHome } from "react-icons/go";

export default class Footer extends React.Component{

  render(){
    return(
      <div className="Footer">
        <div>
          <Link to='/'><img src={logo} alt=""/></Link>
        </div>
        <div>
            <GoHome /> Endereço: Rua xxxxxxxxxxxxxx<br />
            <FaPhone /> Telefone:(99) 99999-9999 <br />
            <FaMailBulk /> Email: clinvet@clinvet.com.br <p />
        </div>
        <div class="row mx-md-n5 Footersocial">
          <div class="col py-3 px-md-3"><Link to ="/"><FaFacebook /></Link></div>
          <div class="col py-3 px-md-3"><Link to ="/"><IoLogoWhatsapp/></Link></div>
          <div class="col py-3 px-md-3"><Link to ="/"><IoLogoTwitter /></Link></div>
        </div>
      </div>
    );
  }
}