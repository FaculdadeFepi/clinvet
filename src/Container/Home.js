import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import Carousel from '../ui/Carousel';
import HomeCard from '../Container/HomeCard';

export default class Home extends Component {

    render() {
        return (
            <div>
                <div>
                    <Carousel />
                    <HomeCard />
                </div>
            </div>
            
        );
    }
}