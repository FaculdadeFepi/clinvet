import React from 'react';
import { MDBRow, MDBCol, MDBCard, MDBIcon } from "mdbreact";
import 'bootstrap/dist/css/bootstrap.min.css';
import produto1 from '../images/Produtos/produto1.png';
import produto2 from '../images/Produtos/produto2.png';
import produto3 from '../images/Produtos/produto3.png';
import produto5 from '../images/Produtos/produto5.png';

import Racao from '../images/Produtos/ração-seca.png';
import Medicamentos from '../images/Produtos/medicamentos.png';
import Petiscos from '../images/Produtos/petiscos.png';
import Tapetes from '../images/Produtos/tapetes.png';

const Store = () => {
    return (
      <section className="text-center my-5">
        <h2 className="h1-responsive font-weight-bold text-center my-5">
          Produtos a venda
        </h2>
        <p className="grey-text text-center w-responsive mx-auto mb-5">
          Encontre aqui alguns produtos que estão a venda em nossa loja.
        </p>
        <MDBRow>
          <MDBCol lg="3" md="6" className="mb-lg-0 mb-4">
            <MDBCard collection className="z-depth-1-half">
              <div className="view zoom">
                <img
                  src={produto1}
                  className="img-fluid"
                  alt=""
                />
                <div className="stripe dark">
                  <a href="#!">
                    <p>
                    Golden <MDBIcon icon="angle-right" />
                    </p>
                  </a>
                </div>
              </div>
            </MDBCard>
          </MDBCol>
          <MDBCol lg="3" md="6" className="mb-lg-0 mb-4">
            <MDBCard collection className="z-depth-1-half">
              <div className="view zoom">
                <img
                  src={produto2}
                  className="img-fluid"
                  alt=""
                />
                <div className="stripe dark">
                  <a href="#!">
                    <p>
                      Royal Canin <MDBIcon icon="angle-right" />
                    </p>
                  </a>  
                </div>
              </div>
            </MDBCard>
          </MDBCol>
          <MDBCol lg="3" md="6" className="mb-lg-0 mb-4">
            <MDBCard collection className="z-depth-1-half">
              <div className="view zoom">
                <img
                  src={produto3}
                  className="img-fluid"
                  alt=""
                />
                <div className="stripe dark">
                  <a href="#!">
                    <p>
                    Bravecto <MDBIcon icon="angle-right" />
                    </p>
                  </a>
                </div>
              </div>
            </MDBCard>
          </MDBCol>
          <MDBCol lg="3" md="6" className="mb-lg-0 mb-4">
            <MDBCard collection className="z-depth-1-half">
              <div className="view zoom">
                <img
                  src={produto5}
                  className="img-fluid"
                  alt=""
                />
                <div className="stripe dark">
                  <a href="#!">
                    <p>
                    Hills <MDBIcon icon="angle-right" />
                    </p>
                  </a>
                </div>
              </div>
            </MDBCard>
          </MDBCol>
        </MDBRow>
        <section className="text-center my-4">
        <MDBRow>
          <MDBCol lg="3" md="6" className="mb-lg-0 mb-4">
            <MDBCard collection className="z-depth-1-half">
              <div className="view zoom">
                <img
                  src={Racao}
                  className="img-fluid"
                  alt=""
                />
                <div className="stripe dark">
                  <a href="#!">
                    <p>
                    Rações <MDBIcon icon="angle-right" />
                    </p>
                  </a>
                </div>
              </div>
            </MDBCard>
          </MDBCol>
          
          <MDBCol lg="3" md="6" className="mb-lg-0 mb-4">
            <MDBCard collection className="z-depth-1-half">
              <div className="view zoom">
                <img
                  src={Medicamentos}
                  className="img-fluid"
                  alt=""
                />
                <div className="stripe dark">
                  <a href="#!">
                    <p>
                    Medicamentos <MDBIcon icon="angle-right" />
                    </p>
                  </a>
                </div>
              </div>
            </MDBCard>
          </MDBCol>
          <MDBCol lg="3" md="6" className="mb-lg-0 mb-4">
            <MDBCard collection className="z-depth-1-half">
              <div className="view zoom">
                <img
                  src={Petiscos}
                  className="img-fluid"
                  alt=""
                />
                <div className="stripe dark">
                  <a href="#!">
                    <p>
                    Petiscos<MDBIcon icon="angle-right" />
                    </p>
                  </a>
                </div>
              </div>
            </MDBCard>
          </MDBCol>
          <MDBCol lg="3" md="6" className="mb-lg-0 mb-4">
            <MDBCard collection className="z-depth-1-half">
              <div className="view zoom">
                <img
                  src={Tapetes}
                  className="img-fluid"
                  alt=""
                />
                <div className="stripe dark">
                  <a href="#!">
                    <p>
                    Tapetes <MDBIcon icon="angle-right" />
                    </p>
                  </a>
                </div>
              </div>
            </MDBCard>
          </MDBCol>
        </MDBRow>
        </section>
      </section>
    );
  }
  
  export default Store;
