
import React, { GoogleMap } from 'react';

const Mapa = () => {
    return (
        <div>
            <GoogleMap
                ref={this.props.onMapLoad}
                defaultZoom={3}
                defaultCenter={{ lat: -25.363882, lng: 131.044922 }}
                onClick={this.props.onMapClick}
                >
            </GoogleMap>
        </div>
    )
};
 export default Mapa;
