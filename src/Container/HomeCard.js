import React, { Component } from 'react';
//import { CardDeck, Card } from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import banho from '../images/HomeCard/banho.fw.png';
import petstore from '../images/HomeCard/petstore.fw.png';
import clinvet from '../images/HomeCard/clinvet.fw.png';



export default class HomeCard extends Component {

    render() {
        return (
            <div className="row justify-content-center"  style={{padding: '4px'}}>
                <div class="mx-4 card col-sm-3">
                    <img class="card-img-top" src={banho} alt="Imagem de capa do card" />
                    <div class="card-body">
                        <h5 class="card-title text-center" style={{ color: 'blue' }}>BANHO E TOSA</h5>
                        <p class="card-text text-left">Aqui no Petshop o tradicional banho e tosa está integrado a um incrível Centro de Estética.
                            Imaginado de um jeitinho diferente, muito mais personalizado para o seu pet sair renovado do pet shop.</p>
                    </div>
                </div>
                <div class="mx-4 card col-sm-3">
                    <img class="card-img-top" src={petstore} alt="Imagem de capa do card" />
                    <div class="card-body">
                        <h5 class="card-title text-center" style={{ color: 'blue' }}>PRODUTOS PET</h5>
                        <p class="card-text text-left">Tá difícil de achar aquele item tão desejado pelo seu pet? No Jubapet a variedade 
                            de produtos vai te impressionar, são mais de 3.000 produtos entre rações, acessórios e muito mais!</p>
                    </div>
                </div>
                <div class="card col-sm-3">
                    <img class="card-img-top" src={clinvet} alt="Imagem de capa do card" />
                    <div class="card-body">
                        <h5 class="card-title text-center" style={{ color: 'blue' }}>CLÍNICA</h5>
                        <p class="card-text text-left">Se precisar cuidar da saúde do seu pet, aqui no Jubapet contamos 
                            com um Centro Veterinário completo, além de vacinas e consultas, conta também com um Centro Cirúrgico.</p>
                    </div>
                </div>
            </div>
        );
    }
}